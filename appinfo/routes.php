<?php

return [
	'routes' => [
		['name' => 'page#index', 'url' => '/', 'verb' => 'GET'],
		['name' => 'DataRequest#export', 'url' => '/api/v1/export', 'verb' => 'POST'],
	]
];
