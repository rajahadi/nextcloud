<?php

namespace OCA\Organization\AppInfo;

use OCP\AppFramework\App;

$app = new App('organization');
$container = $app->getContainer();

$container->query('OCP\INavigationManager')->add(function () use ($container) {
	$urlGenerator = $container->query('OCP\IURLGenerator');
	$l10n = $container->query('OCP\IL10N');
	return [
		// the string under which your app will be referenced in Nextcloud
		'id' => 'organization',

		// sorting weight for the navigation. The higher the number, the higher
		// will it be listed in the navigation
		'order' => 12,

		// the route that will be shown on startup
		'href' => $urlGenerator->linkToRoute('organization.page.index'),

		// the icon that will be shown in the navigation
		// this file needs to exist in img/
		'icon' => $urlGenerator->imagePath('organization', 'app.svg'),

		// the title of your application. This will be used in the
		// navigation or on the settings page of your app
		'name' => $l10n->t('Organization'),
	];
});

