(function (OC, window, $, undefined) {
    'use strict';

    $(document).ready(function () {

        var translations = {
            newOrganization: $('#new-organization-string').text()
        };

// this notes object holds all our notes
        var Organizations = function (baseUrl) {
            this._baseUrl = baseUrl;
            this._organizations = [];
            this._activeOrganizaiton = undefined;
        };

        Organizations.prototype = {
            load: function (id) {
                var self = this;
                this._organizations.forEach(function (organization) {
                    if (organization.id === id) {
                        organization.active = true;
                        self._activeOrganization = organization;
                    } else {
                        organization.active = false;
                    }
                });
            },
            getActive: function () {
                return this._activeOrganization;
            },
            removeActive: function () {
                var index;
                var deferred = $.Deferred();
                var id = this._activeOrganization.id;
                this._organizations.forEach(function (organization, counter) {
                    if (organization.id === id) {
                        index = counter;
                    }
                });

                if (index !== undefined) {
                    // delete cached active note if necessary
                    if (this._activeOrganization === this._organizations[index]) {
                        delete this._activeOrganization;
                    }

                    this._organizations.splice(index, 1);

                    $.ajax({
                        url: this._baseUrl + '/' + id,
                        method: 'DELETE'
                    }).done(function () {
                        deferred.resolve();
                    }).fail(function () {
                        deferred.reject();
                    });
                } else {
                    deferred.reject();
                }
                return deferred.promise();
            },
            create: function (organization) {
                var deferred = $.Deferred();
                var self = this;
                $.ajax({
                    url: this._baseUrl,
                    method: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify(organization)
                }).done(function (organization) {
                    self._organizations.push(organization);
                    self._activeOrganization = organization;
                    self.load(organization.id);
                    deferred.resolve();
                }).fail(function () {
                    deferred.reject();
                });
                return deferred.promise();
            },
            getAll: function () {
                return this._organizations;
            },
            loadAll: function () {
                var deferred = $.Deferred();
                var self = this;
                $.get(this._baseUrl).done(function (organizations) {
                    self._activeOrganization = undefined;
                    self._organizations = organizations;
                    deferred.resolve();
                }).fail(function () {
                    deferred.reject();
                });
                return deferred.promise();
            },
            updateActive: function (title, content) {
                var organization = this.getActive();
                organization.organization_name = title;
                organization.organization_sub_detail = content;

                return $.ajax({
                    url: this._baseUrl + '/' + organization.id,
                    method: 'PUT',
                    contentType: 'application/json',
                    data: JSON.stringify(organization)
                });
            }
        };

// this will be the view that is used to update the html
        var View = function (organizations) {
            this._organizations = organizations;
        };

        View.prototype = {
            renderContent: function () {
                var source = $('#content-tpl').html();
                var template = Handlebars.compile(source);
                var html = template({organization: this._organizations.getActive()});

                $('#editor').html(html);

                // handle saves
                var textarea = $('#app-content textarea');
                var self = this;
                $('#app-content button').click(function () {
                    var content = textarea.val();
                    var title = content.split('\n')[0]; // first line is the title

                    self._organizations.updateActive(title, content).done(function () {
                        self.render();
                    }).fail(function () {
                        alert('Could not update organization, not found');
                    });
                });
            },
            renderNavigation: function () {
                var source = $('#navigation-tpl').html();
                var template = Handlebars.compile(source);
                var html = template({organizations: this.organizations.getAll()});

                $('#app-navigation ul').html(html);

                // create a new note
                var self = this;
                $('#new-organization').click(function () {
                    var organization = {
                        title: translations.newOrganization,
                        content: ''
                    };

                    self._organizations.create(organization).done(function() {
                        self.render();
                        $('#editor textarea').focus();
                    }).fail(function () {
                        alert('Could not create organization');
                    });
                });

                // show app menu
                $('#app-navigation .app-navigation-entry-utils-menu-button').click(function () {
                    var entry = $(this).closest('.organization');
                    entry.find('.app-navigation-entry-menu').toggleClass('open');
                });

                // delete a note
                $('#app-navigation .organization .delete').click(function () {
                    var entry = $(this).closest('.organization');
                    entry.find('.app-navigation-entry-menu').removeClass('open');

                    self._organizations.removeActive().done(function () {
                        self.render();
                    }).fail(function () {
                        alert('Could not delete organization, not found');
                    });
                });

                // load a note
                $('#app-navigation .organization > a').click(function () {
                    var id = parseInt($(this).parent().data('id'), 10);
                    self._organizations.load(id);
                    self.render();
                    $('#editor textarea').focus();
                });
            },
            render: function () {
                this.renderNavigation();
                this.renderContent();
            }
        };

        var organizations = new Organizations(OC.generateUrl('/apps/organization/organizations'));
        var view = new View(organizations);
        organizations.loadAll().done(function () {
            view.render();
        }).fail(function () {
            alert('Could not load organizations');
        });


    });

})(OC, window, jQuery);
