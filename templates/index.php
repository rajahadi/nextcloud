<?php
script('organization', 'handlebars');
script('organization', 'script');
style('organization', 'style');
?>

<div id="app">
	<div id="app-navigation">
		<?php print_unescaped($this->inc('navigation/index')); ?>
		<?php print_unescaped($this->inc('settings/index')); ?>
	</div>

	
</div>

<div id="app-content">
     <div id="app-content-wrapper">
              <?php print_unescaped($this->inc('content/index')); ?>
     </div>
</div>
